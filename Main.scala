//> using dep "com.wbillingsley::doctacular::0.3.0"

import com.wbillingsley.veautiful.html.*
import com.wbillingsley.veautiful.doctacular.*

case class Counter() extends DHtmlComponent { 
  val count = stateVariable(0)

  override def render = div(
    button("Increment", ^.on.click --> { count.value += 1 }),
    span(s"I've been clicked ${count.value} times")
  )
}

val root = mountToBody(div("Loading..."))

@main def main = 
  // Now just script and mount the deck
  DeckBuilder(1280, 720)
    .veautifulSlide(
        div(
          h1("A slide desck from Scala!"),
          p("Just a little demo of what you can do with Doctacular"),
        )
    ).withClass("center middle")
    .veautifulSlide(
        div(
          h1("Interactive content"),
          p("The world's smallest demo..."),
          Counter()
        )
    )
    .markdownSlide(
        """
        |# Markdown slides
        |## Just write your slides in Markdown
        |""".stripMargin
    )
    .mountToRoot(root)

