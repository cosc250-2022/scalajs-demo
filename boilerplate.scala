import com.wbillingsley.veautiful.html.*

import scala.scalajs.js
import scala.scalajs.js.annotation._

@js.native
@JSGlobal("marked")
object Marked extends js.Object:
  def parse(s:String):String = js.native

given markdown:Markup = new Markup({ (s:String) => Marked.parse(s).asInstanceOf[String] })
